using DNSAddress.Domain.Domain.Model.ServiceModel;
using DNSAddress.Domain.Domain.Services;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Threading.Tasks;

namespace DNSAddress.Rdap.Api.Test
{
    public class RdapTests
    {
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// GetRdapTests
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetRdapTests()
        {
            try
            {
                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                DNSAddressService dnsAddressService = new DNSAddressService(config);
                var result = await dnsAddressService.GetDNSAddressInfo<RdapGetResponse>(new BaseServiceRequest() { IpAddress = "8.8.8.8" });
                if (result.IsSuccessful)
                {
                    Assert.True(true);
                }
                else
                {
                    Assert.Fail(result.ErrorMessage);
                }
            }
            catch (System.Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}