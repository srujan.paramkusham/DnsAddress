using DNSAddress.Domain.Domain.Model.ServiceModel;
using DNSAddress.Domain.Domain.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using NUnit.Framework;
using System.Threading.Tasks;

namespace DNSAddress.GeoIp.Api.Test
{
    public class GeoIPTests
    {      

        [SetUp]
        public void Setup()
        {
         
        }

        /// <summary>
        /// GetIPApiTests
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetGeoIpTests()
        {
            try
            {

                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                DNSAddressService dnsAddressService = new DNSAddressService(config);
                var result = await dnsAddressService.GetDNSAddressInfo<GeoIPGetResponse>(new BaseServiceRequest() { IpAddress = "8.8.8.8" });               
                if (result.IsSuccessful)
                {
                    Assert.True(true);
                }
                else
                {
                    Assert.Fail(result.ErrorMessage);
                }
            }
            catch (System.Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// GetIPStackTests
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetIPStackTests()
        {
            try
            {
                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                DNSAddressService dnsAddressService = new DNSAddressService(config);
                var result = await dnsAddressService.GetDNSAddressInfo<IPStackGetResponse>(new BaseServiceRequest() { IpAddress = "8.8.8.8" });               
                if (result.IsSuccessful)
                {
                    Assert.True(true);
                }
                else
                {
                    Assert.Fail(result.ErrorMessage);
                }
            }
            catch (System.Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }


        /// <summary>
        /// GetIPApiTests
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task GetIPApiTests()
        {
            try
            {
                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                DNSAddressService dnsAddressService = new DNSAddressService(config);
                var result = await dnsAddressService.GetDNSAddressInfo<IPApiGetResponse>(new BaseServiceRequest() { IpAddress = "8.8.8.8" });
                if (result.IsSuccessful)
                {
                    Assert.True(true);
                }
                else
                {
                    Assert.Fail(result.ErrorMessage);
                }
            }
            catch (System.Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}