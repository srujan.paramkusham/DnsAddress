﻿using DNSAddress.Domain.Domain.Model.ServiceModel;
using DNSAddress.Domain.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DNSAddress.Rdap.Api.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class RdapController : ControllerBase
    {      
        private readonly ILogger<RdapController> _logger;
        private readonly IDNSAddressService _dnsAddressService;

        public RdapController( ILogger<RdapController> logger,
                               IDNSAddressService dnsAddressService)
        {           
            _logger = logger;
            _dnsAddressService = dnsAddressService;
        }
       

        /// <summary>
        /// Get Rdap
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Rdap")]
        public async Task<RdapGetResponse> Rdap(string IpAddress)
        {
            var result = await _dnsAddressService.GetDNSAddressInfo<RdapGetResponse>(new BaseServiceRequest() { IpAddress = IpAddress });
            return result;
        }
    }
}
