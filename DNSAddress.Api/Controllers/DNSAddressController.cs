﻿using DNSAddress.Domain.Domain.Model.ServiceModel;
using DNSAddress.Domain.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNSAddress.Api.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class DNSAddressController : ControllerBase
    {       
        private readonly ILogger<DNSAddressController> _logger;      
        private readonly IDNSAddressService _dnsAddressService;

        public DNSAddressController(ILogger<DNSAddressController> logger,
                               IDNSAddressService dnsAddressService)
        {          
            _logger = logger;
            _dnsAddressService = dnsAddressService;
        }

        /// <summary>
        /// Retrive data for given ip address by providing service as combination of "geoip,ipstack,ipapi,rdap"
        /// </summary>
        /// <param name="service"></param>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDNSAddressInfo")]
        [SwaggerOperation(Description = "It gives information about dns/addrss for provided services, service parameter should be combination of 'geoip,ipstack,ipapi,rdap'.")]
        public async Task<DNSAddressGetResponse> GetDNSAddressInfo(string service , string IpAddress)
        {
            DNSAddressGetResponse response = new DNSAddressGetResponse();

            if (string.IsNullOrEmpty(service))
            {
                response.IsSuccessful = false;
                response.ErrorMessage = "Please provide service list.";
            }

            var result = await _dnsAddressService.GetDNSAddress(new DNSAddressGetRequest() {ServiceList = service, IpAddress = IpAddress });
            return result;
        }
        
    }
}
