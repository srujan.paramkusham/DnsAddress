# Dns Address

Create an application that integrates multiple services into a useful and timesaving workflow.
The current code implements the solution as per the objectives.

## Instructions

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:d9f4bc20338b1f1fab273ce64c258d5d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:d9f4bc20338b1f1fab273ce64c258d5d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:d9f4bc20338b1f1fab273ce64c258d5d?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/srujan.paramkusham/DnsAddress.git
git branch -M main
git push -uf origin main
```

## Objectives

Build an application in Typescript that provides an API that takes an IP or domain as input and gathers information from multiple sources returning a single result. Your application must farm individual portions of the lookup to various workers who perform the action. The application should then combine the results and return a single payload.

The API should accept:
- IP or domain
- A list of services to query. This input should be optional with a default list used if none provided

The API should then:
- Validate input
- Break the request into multiple individual tasks
- Send those tasks to a pool of 2+ workers. The API must support workers on separate machines/docker containers than the API.
- The workers should then perform the tasks and return the results to the application
- The application should wait for all tasks for a request to be completed, combine the results and return

Some suggested services available to query along with the respective APIs: 
- GeoIP
  - https://freegeoip.app/json/8.8.8.8?callback=test
  - https://api.ipstack.com/134.201.250.155?access_key=YOUR_ACCESS_KEY&callback=MY_FUNCTION
  - http://ip-api.com/json/24.48.0.1
- RDAP
  - https://rdap.arin.net/registry/ip/8.8.8.8
- Reverse DNS
  - https://rapidapi.com/whoisapi/api/dns-lookup/

These are APIs needs to be implemented as standalone services and deploy them in the separate docker containers to simulate the gathers information from multiple sources returning a single result.

Bonus Points:
- API has a Swagger Spec (if you use REST)
- Support partial results on error or rate limit

What we are looking for:
- How you organize your project
- Usage of Microservices
- What tooling you use (NUnit/xunit with mocked objects)
- Creativity and performance


### Design
 
<img alt="DnsAddressApplication.JPG" class="js-lazy-loaded qa-js-lazy-loaded" src="https://gitlab.com/srujan.paramkusham/DnsAddress/-/raw/main/Documents/DnsAddressApplication.JPG" loading="lazy">

<img alt="DnsAddressDocker.JPG" class="js-lazy-loaded qa-js-lazy-loaded" src="https://gitlab.com/srujan.paramkusham/DnsAddress/-/raw/main/Documents/DnsAddressDocker.JPG" loading="lazy">

### Get DNA Address Info

- `GET /api/v1/DNSAddress/GetDNSAddressInfo?service={service}&IpAddress={IpAddress}` -> Returns JSON results.

   **Parameter** - `service`

  - Type `{string}` - Comma separated list of strings
    - It accepatable following values:
      - `geoip`   - GeoIP
      - `ipapi`   - IpApi
      - `ipstack` - IPStack
      - `rdap` 	  - RDAP    

    - Example: `geoip,rdap`

  **Parameter** - `IpAddress`

  - Type `{string}` - IP Address or Domain
    - Example: `8.8.8.8` or `google.com`

### Prerequisites

- .NET Core SDK 5.0
- Visual Studio 2019

## Running the App in Docker
To Run the application in docker, Excute following command.

```shell
docker-compose build
```
```shell
docker-compose up
```

To stop the containers run following command-line

```shell
docker-compose down
```

Create an application that integrates multiple services into a useful and timesaving workflow.
The current code implements the solution as per the objectives.

### ## Swagger Application URL's

- [API](http://localhost:5000/swagger/)
- [GeoIP API](http://localhost:5001/swagger/)
- [RDAP API](http://localhost:5002/swagger/)

