﻿using DNSAddress.Domain.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNSAddress.GeoIp.Api.Extensions
{
    public static class ServiceExtensions
    {
        public static object CommonConstants { get; private set; }

        public static void AddSwaggerExtension(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DNSAddress.GeoIp.Api", Version = "v1" });
            });
        }

        public static void AddApiVersionExtension(this IServiceCollection services)
        {
            services.AddApiVersioning(x =>
            {
                x.DefaultApiVersion = new ApiVersion(1, 0);
                x.AssumeDefaultVersionWhenUnspecified = true;
                x.ReportApiVersions = true;
                x.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
            });
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IDNSAddressService, DNSAddressService>();
        }
    }
}
