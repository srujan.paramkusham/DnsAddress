﻿using DNSAddress.Domain.Domain.Model.ServiceModel;
using DNSAddress.Domain.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNSAddress.GeoIp.Api.Controllers
{
    /// <summary>
    /// GeoIPController
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class GeoIPController : ControllerBase
    {
        private readonly ILogger<GeoIPController> _logger;
        private readonly IDNSAddressService _dnsAddressService;

        public GeoIPController(ILogger<GeoIPController> logger,
                               IDNSAddressService dnsAddressService)
        {
            _logger = logger;
            _dnsAddressService = dnsAddressService;
        }

        /// <summary>
        /// GetGeoIP
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GeoIP")]
        public async Task<GeoIPGetResponse> GeoIP(string IpAddress)
        {
            var result = await _dnsAddressService.GetDNSAddressInfo<GeoIPGetResponse>(
                new BaseServiceRequest() { IpAddress = IpAddress });
            return result;
        }

        /// <summary>
        /// GetIPApi
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("IPApi")]
        public async Task<IPApiGetResponse> IPApi(string IpAddress)
        {
             var result = await _dnsAddressService.GetDNSAddressInfo<IPApiGetResponse>(
                new BaseServiceRequest { IpAddress = IpAddress });
            return result;
        }

        /// <summary>
        /// GetIPStack
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("IPStack")]
        public async Task<IPStackGetResponse> IPStack(string IpAddress)
        {
            var result = await _dnsAddressService.GetDNSAddressInfo<IPStackGetResponse>(
                new BaseServiceRequest { IpAddress = IpAddress });

            return result;
        }
    }
}
