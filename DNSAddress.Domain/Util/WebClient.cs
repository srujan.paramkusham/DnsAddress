﻿using DNSAddress.Domain.Domain.Model.ServiceModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DNSAddress.Domain.Util
{
    public class WebClient
    {
        /// <summary>
        /// HttpClient
        /// </summary>
        private readonly HttpClient _client = new HttpClient();

        /// <summary>
        /// GetWebAsync
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="Url"></param>
        /// <returns></returns>
        public async Task<TResponse> GetWebAsync<TResponse>(string Url) where TResponse : BaseServiceResponse
        {
            var result = new HttpResponseMessage();
            var response = new BaseServiceResponse()
            {
                IsSuccessful = false,
                ErrorMessage = "Error occured in GetWebAsync"
            };


            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                client.BaseAddress = new Uri(Url);
                result = await client.GetAsync(Url);
                if (result.IsSuccessStatusCode)
                {
                    response = JsonConvert.DeserializeObject<TResponse>(await result.Content.ReadAsStringAsync());                   
                    response.IsSuccessful = true;
                    response.ErrorMessage = "";
                }
            }

            return (TResponse)response;
        }
        
    }
}
