﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DNSAddress.Domain.Util
{
    public static class ValidateHost
    {
        /// <summary>
        /// Validate Host Address
        /// </summary>
        /// <param name="hostname"></param>
        /// <returns></returns>
        public static bool ValidateHostAddress(string hostname)
        {
            bool returnValue = false;

            try
            {
                string HostName = hostname;
                IPAddress[] ipaddress = Dns.GetHostAddresses(hostname);
                if (ipaddress.Count() > 0)
                {
                    returnValue = true;
                }
            }
            catch (Exception)
            {
                returnValue = false;
            }

            return returnValue;
        }
    }
}
