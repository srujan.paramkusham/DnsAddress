﻿
using DNSAddress.Domain.Domain.Model.ServiceModel;
using DNSAddress.Domain.Util;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNSAddress.Domain.Domain.Services
{
    public class DNSAddressService : IDNSAddressService
    {

        /// <summary>
        /// IConfiguration
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// WebClient
        /// </summary>
        private readonly WebClient _webClient;

        /// <summary>
        /// CombineIPService
        /// </summary>
        /// <param name="configuration"></param>
        public DNSAddressService(IConfiguration configuration)
        {
            _configuration = configuration;
            _webClient = new WebClient();
        }


        /// <summary>
        /// GetDNSAddressInfo
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<TResponse> GetDNSAddressInfo<TResponse>(BaseServiceRequest request) where TResponse : BaseServiceResponse
        {
            Type type = typeof(TResponse);
            dynamic response = Activator.CreateInstance(type);          

            try
            {
                if (string.IsNullOrEmpty(request.IpAddress))
                {
                    response.IsSuccessful = false;
                    response.ErrorMessage = "Ip Address should not be empty";
                    return (TResponse)response;
                }

                var validateHost = ValidateHost.ValidateHostAddress(request.IpAddress);
                if (!validateHost)
                {                  
                    response.IsSuccessful = false;
                    response.ErrorMessage = "Invalid Hostname";

                    //var result = Convert.ChangeType(response, typeof(TResponse));

                    return (TResponse) response;
                }

                string typeName = typeof(TResponse).Name.ToLower();

                if (string.IsNullOrEmpty(request.Url))
                {
                    switch (typeName)
                    {
                        case "geoipgetresponse":
                            request.Url = _configuration.GetSection("ApiUrl").GetSection("GeoIPUrl").Value + request.IpAddress;
                            break;

                        case "ipapigetresponse":
                            request.Url = _configuration.GetSection("ApiUrl").GetSection("IpApiUrl").Value + request.IpAddress;
                            break;

                        case "ipstackgetresponse":
                            request.Url = _configuration.GetSection("ApiUrl").GetSection("IPStackApiUrl").Value + request.IpAddress + "?access_key="
                                                  + _configuration.GetSection("ApiUrl").GetSection("IPStackApiUrlKey").Value;
                            break;

                        case "rdapgetresponse":
                            request.Url = _configuration.GetSection("ApiUrl").GetSection("RDAPUrl").Value + request.IpAddress;
                            break;

                        default:
                            break;
                    }
                }

                response = await _webClient.GetWebAsync<TResponse>(request.Url);
                response.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.ErrorMessage = ex.Message;
            }

            return (TResponse)response;
        }


        /// <summary>
        /// GetDNSAddress
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DNSAddressGetResponse> GetDNSAddress(DNSAddressGetRequest request)
        {
            DNSAddressGetResponse response = new DNSAddressGetResponse()
            {
                DNSAddressJson = new System.Collections.Generic.List<object>()
            };           

            request.listOfServices = request.ServiceList
           .Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
           .Select(s => s.Trim())
           .Distinct().ToList();

            var tasks = new List<Task>();

            dynamic resultGeoIp =  null;
            dynamic resultIpApi = null;
            dynamic resultIpStack = null;
            dynamic resultRdap = null;

            foreach (string service in request.listOfServices)
            {
                string ServiceName = service.ToLower();
                switch (service)
                {
                    case "geoip":
                        resultGeoIp = GetGeoIp(request.IpAddress);
                        response.DNSAddressJson.Add(resultGeoIp.Result);
                        tasks.Add(resultGeoIp);
                        break;
                    case "ipapi":
                         resultIpApi = GetIPApi(request.IpAddress);
                        response.DNSAddressJson.Add(resultIpApi.Result);
                        tasks.Add(resultIpApi);
                        break;
                    case "ipstack":
                        resultIpStack = GetIPStack(request.IpAddress);
                        response.DNSAddressJson.Add(resultIpStack.Result);
                        tasks.Add(resultIpStack);
                        break;
                    case "rdap":
                        resultRdap = GetRdapIp(request.IpAddress);
                        response.DNSAddressJson.Add(resultRdap.Result);
                        tasks.Add(resultRdap);
                        break;
                    default:
                        break;
                }
            }

            await Task.WhenAll(tasks);

            response.IsSuccessful = true;
            return response;
        }

        /// <summary>
        /// GetGeoIp , This method used to call inside docker container GeoIp services
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        private async Task<GeoIPGetResponse> GetGeoIp(string IpAddress)
        {       
            var url = _configuration.GetSection("ApiUrl").GetSection("GeoIPUrl").Value + "GeoIp?IpAddress=" + IpAddress;
            var result = await GetDNSAddressInfo<GeoIPGetResponse>(
               new BaseServiceRequest() { Url= url, IpAddress = IpAddress });
            return result;
        }

        /// <summary>
        /// GetIPApi, This method used to call inside docker container GetIPApi services
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        private async Task<IPApiGetResponse> GetIPApi(string IpAddress)
        {
            var url = _configuration.GetSection("ApiUrl").GetSection("GeoIPUrl").Value + "IPApi?IpAddress=" + IpAddress;
            var result = await GetDNSAddressInfo<IPApiGetResponse>(
               new BaseServiceRequest() { Url = url, IpAddress = IpAddress });
            return result;
        }

        /// <summary>
        /// GetIPStack, This method used to call inside docker container GetIPStack services
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        private async Task<IPStackGetResponse> GetIPStack(string IpAddress)
        {
            var url = _configuration.GetSection("ApiUrl").GetSection("GeoIPUrl").Value + "IPStack?IpAddress=" + IpAddress;
            var result = await GetDNSAddressInfo<IPStackGetResponse>(
               new BaseServiceRequest() { Url = url, IpAddress = IpAddress });
            return result;
        }

        /// <summary>
        /// GetRdapIp, This method used to call inside docker container GetRdapIp services
        /// </summary>
        /// <param name="IpAddress"></param>
        /// <returns></returns>
        private async Task<RdapGetResponse> GetRdapIp(string IpAddress)
        {
            var url = _configuration.GetSection("ApiUrl").GetSection("RdapIPUrl").Value + "Rdap?IpAddress=" + IpAddress;
            var result = await GetDNSAddressInfo<RdapGetResponse>(
               new BaseServiceRequest() { Url = url, IpAddress = IpAddress });
            return result;
        }
    }
}
