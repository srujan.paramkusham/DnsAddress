﻿using DNSAddress.Domain.Domain.Model.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNSAddress.Domain.Domain.Services
{
    public interface IDNSAddressService
    {
        Task<DNSAddressGetResponse> GetDNSAddress(DNSAddressGetRequest request);
        Task<TResponse> GetDNSAddressInfo<TResponse>(BaseServiceRequest request) where TResponse : BaseServiceResponse;
    }
}
