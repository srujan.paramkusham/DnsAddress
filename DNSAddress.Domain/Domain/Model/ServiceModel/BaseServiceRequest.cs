﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNSAddress.Domain.Domain.Model.ServiceModel
{
    [Serializable]
    public class BaseServiceRequest
    {
        /// <summary>
        /// IpAddress
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// listOfServices
        /// </summary>
        public List<string> listOfServices { get; set; }
    }
}
