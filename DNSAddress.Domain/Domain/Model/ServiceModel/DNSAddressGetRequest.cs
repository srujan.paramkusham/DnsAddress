﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNSAddress.Domain.Domain.Model.ServiceModel
{
    public class DNSAddressGetRequest : BaseServiceRequest
    {
        /// <summary>
        /// ServiceList
        /// </summary>
        public string  ServiceList { get; set; }
    }
}
