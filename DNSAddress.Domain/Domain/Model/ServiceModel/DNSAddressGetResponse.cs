﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DNSAddress.Domain.Domain.Model.ServiceModel
{   
    /// <summary>
    /// DNSAddress Get Response
    /// </summary>
    public class DNSAddressGetResponse : BaseServiceResponse
    { 
        public List<Object> DNSAddressJson { get; set; }
    }
}
