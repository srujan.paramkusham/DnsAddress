﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNSAddress.Domain.Domain.Model.ServiceModel
{
    /// <summary>
    /// Base Service Response
    /// </summary>
    
    public class BaseServiceResponse
    {
        /// <summary>
        /// Whether or not the service operation was successful.
        /// </summary>
        public Boolean IsSuccessful { get; set; }

        /// <summary>
        /// ErrorMessage returned by the service, such as an ErrorMessage if it fails, or details on the success of the operation.
        /// </summary>
        public String ErrorMessage { get; set; }
    }
}
