using DNSAddress.Domain.Domain.Model.ServiceModel;
using DNSAddress.Domain.Domain.Services;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Threading.Tasks;

namespace DNSAddress.Api.Test
{
    public class DNSAddressTests
    {
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// GetDNSAddressInfoTests
        /// This test executes only when docker containers are running
        /// </summary>
        [Test]
        public async Task GetDNSAddressInfoTests()
        {
            try
            {
                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                DNSAddressService dnsAddress = new DNSAddressService(config);              

                var result = await dnsAddress.GetDNSAddress(new DNSAddressGetRequest() { ServiceList = "geoip,rdap,ipapi,ipstack", IpAddress = "8.8.8.8" });
               
                if (result.IsSuccessful)
                {
                    Assert.True(true);
                }
                else
                {
                    Assert.Fail(result.ErrorMessage);
                }
            }
            catch (System.Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}